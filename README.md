![](manual/images/general.png)

# FLIOps #

A package to perform the observations using a FLI CCD camera. The package is
designed to control all the imaging devices attached to our LX-200 Meade telescope.

## List of supported devices ##

 - MicroLine FLI CCD camera
 - Two FLI CFW1-8 filter wheels
 - Gemini focuser-rotator


## Requirements ##

 - Python 3.*
 - pyserial
 - astropy
 - numpy

## Documentation ##

The full [documentation][1] (PDF, Rus)

[1]: https://bitbucket.org/latrop/fliops/src/master/manual/fliops_manual.pdf
