#! /usr/bin/env python

import os
import time

from ctypes import POINTER
from ctypes import byref
from ctypes import sizeof
from ctypes import c_long
from ctypes import c_size_t
from ctypes import c_uint16
from ctypes import c_double
from ctypes import create_string_buffer

from threading import Thread

import numpy

from .errors import DeviceIsNotAvailableError
from .fli_lib import FLILibrary
from .fli_lib import flidomain_t
from .fli_lib import fliframe_t
from .fli_lib import flimode_t
from .fli_lib import FLIDOMAIN_USB
from .fli_lib import FLIDEVICE_CAMERA
from .fli_lib import FLI_FRAME_TYPE_NORMAL
from .fli_lib import FLI_FRAME_TYPE_DARK
from .fli_lib import FLI_TEMPERATURE_CCD
from .fli_lib import FLI_TEMPERATURE_BASE
from .fli_lib import FLI_SHUTTER_OPEN
from .fli_lib import FLI_SHUTTER_CLOSE
from .fli_lib import FLI_BGFLUSH_START
from .fli_lib import FLI_BGFLUSH_STOP

from .fli_device import USBDevice


class USBCamera(USBDevice):
    # load the DLL
    _libfli = FLILibrary.getDll(debug=False)
    _domain = flidomain_t(FLIDOMAIN_USB | FLIDEVICE_CAMERA)

    def __init__(self, dev_name, model):
        USBDevice.__init__(self, dev_name=dev_name, model=model)
        self.hbin = 1
        self.vbin = 1

    def set_signal_sender(self, signal_sender):
        self.signal_sender = signal_sender

    def get_mode_string(self, mode_idx):
        BUFFER_SIZE = 64
        mode_string = create_string_buffer(BUFFER_SIZE)
        self._libfli.FLIGetCameraModeString(self._dev, flimode_t(mode_idx), mode_string, c_size_t(BUFFER_SIZE))
        return mode_string.value

    def set_camera_mode(self, mode):
        self._libfli.FLISetCameraMode(self._dev, flimode_t(mode))

    def get_camera_mode(self):
        mode_value = c_long()
        self._libfli.FLIGetCameraMode(self._dev, byref(mode_value))
        return mode_value.value

    def get_image_size(self):
        "returns (row_width, img_rows, img_size)"
        left, top, right, bottom = (c_long(), c_long(), c_long(), c_long())
        self._libfli.FLIGetVisibleArea(self._dev, byref(left), byref(top), byref(right), byref(bottom))
        row_width = (right.value - left.value) / self.hbin
        img_rows = (bottom.value - top.value) / self.vbin
        img_size = img_rows * row_width * sizeof(c_uint16)
        return (row_width, img_rows, img_size)

    def set_image_binning(self, hbin=1, vbin=1):
        left, top, right, bottom = (c_long(), c_long(), c_long(), c_long())
        self._libfli.FLIGetVisibleArea(self._dev, byref(left), byref(top), byref(right), byref(bottom))
        row_width = int((right.value - left.value) / hbin)
        img_rows = int((bottom.value - top.value) / vbin)
        self._libfli.FLISetImageArea(self._dev, left, top, left.value + row_width, top.value + img_rows)
        self._libfli.FLISetHBin(self._dev, c_long(hbin))
        self._libfli.FLISetVBin(self._dev, c_long(vbin))
        self.hbin = hbin
        self.vbin = vbin

    def set_flushes(self, num):
        """
        set the number of flushes to the CCD before taking exposure
        must have 0 <= num <= 16, else raises ValueError
        """
        if not(0 <= num <= 16):
            raise ValueError("must have 0 <= num <= 16")
        self._libfli.FLISetNFlushes(self._dev, c_long(num))

    def control_background_flushing(self, new_state):
        if new_state is True:
            ret_code = self._libfli.FLIControlBackgroundFlush(self._dev, FLI_BGFLUSH_START)
        elif new_state is False:
            ret_code = self._libfli.FLIControlBackgroundFlush(self._dev, FLI_BGFLUSH_STOP)
        print("BGFlush setting ret code: %i" % int(ret_code))

    def set_temperature(self, T):
        "set the camera's temperature target in degrees Celcius"
        self._libfli.FLISetTemperature(self._dev, c_double(T))

    def get_temperature(self):
        "gets the camera's temperature in degrees Celcius"
        T = c_double()
        self._libfli.FLIGetTemperature(self._dev, byref(T))
        return T.value

    def read_CCD_temperature(self):
        "gets the CCD's temperature in degrees Celcius"
        T = c_double()
        self._libfli.FLIReadTemperature(self._dev, FLI_TEMPERATURE_CCD, byref(T))
        return T.value

    def read_base_temperature(self):
        "gets the cooler's hot side in degrees Celcius"
        T = c_double()
        self._libfli.FLIReadTemperature(self._dev, FLI_TEMPERATURE_BASE, byref(T))
        return T.value

    def get_cooler_power(self):
        "gets the cooler's power in watts (undocumented API function)"
        P = c_double()
        self._libfli.FLIGetCoolerPower(self._dev, byref(P))
        return P.value

    def open_shutter(self):
        "opens the camera's shutter"
        ret_code = self._libfli.FLIControlShutter(self._dev, FLI_SHUTTER_OPEN)
        print("shutter ret_code: %i" % ret_code)

    def close_shutter(self):
        "closes the camera's shutter"
        ret_code = self._libfli.FLIControlShutter(self._dev, FLI_SHUTTER_CLOSE)
        print("shutter ret_code: %i" % ret_code)

    def set_exposure(self, exptime, frametype="normal"):
        """setup the exposure type:
               exptime   - exposure time in milliseconds
               frametype -  'normal'     - open shutter exposure
                            'dark'       - exposure with shutter closed
        """
        self.current_frametype = frametype
        exptime = c_long(int(exptime))
        if frametype == "normal":
            frametype = fliframe_t(FLI_FRAME_TYPE_NORMAL)
        elif frametype == "dark":
            frametype = fliframe_t(FLI_FRAME_TYPE_DARK)
        else:
            raise ValueError("'frametype' must be either 'normal','dark'")
        self._libfli.FLISetExposureTime(self._dev, exptime)
        self._libfli.FLISetFrameType(self._dev, frametype)

    def take_photo(self, wait_shutter=True, camera_status_value=None, emit_signal=False):
        """
        Expose the frame, wait for completion, and fetch the image data.
        """
        self.exposure_stopped = False
        self.start_exposure()
        if camera_status_value is not None:
            camera_status_value.set("Taking %s exposure" % self.current_frametype)
        # wait for completion
        while True:
            time.sleep(0.25)
            timeleft = self.get_exposure_timeleft()
            if timeleft == 0:
                break
        # grab the image
        if camera_status_value is not None:
            camera_status_value.set("Fetching image")
        image = self.fetch_image(wait_shutter, emit_signal=emit_signal)
        if camera_status_value is not None:
            camera_status_value.set("Idle")
        if self.exposure_stopped is True:
            # User hit Abort button during exposure taking or fetching processes.
            # In this case we just return None instead of the image. The flushed
            # Image is just being rejected here: the fetching is used only for
            # the purpose of CCD cleaning
            return None
        # Image taking is completed successfully
        return image

    def start_exposure(self):
        """ Begin the exposure and return immediately.
            Use the method  'get_timeleft' to check the exposure progress
            until it returns 0, then use method 'fetch_image' to fetch the image
            data as a numpy array.
        """
        self._libfli.FLIExposeFrame(self._dev)

    def cancel_exposure(self):
        """
        Cancels exposure by closing the shutter
        """
        self._libfli.FLICancelExposure(self._dev)

    def get_exposure_timeleft(self):
        """ Returns the time left on the exposure in milliseconds.
        """
        timeleft = c_long()
        self._libfli.FLIGetExposureStatus(self._dev, byref(timeleft))
        return timeleft.value

    def fetch_image(self, wait_shutter=True, emit_signal=False):
        """
        Fetch the image data for the last exposure. Returns a numpy.ndarray object.
        """
        if wait_shutter:
            time.sleep(0.5)  # wait for shutter to close.
        if emit_signal is True:
            # Send a zmq signal to tell that the shutter is closed
            Thread(target=self.signal_sender.send_signal, args=("dither", )).start()
        row_width, img_rows, img_size = self.get_image_size()
        row_width = int(row_width)
        img_rows = int(img_rows)
        # allocate numpy array to store image
        img_array = numpy.zeros((img_rows, row_width), dtype=numpy.uint16)
        # get pointer to array's data space
        img_ptr = img_array.ctypes.data_as(POINTER(c_uint16))
        # grab image buff row by row
        row_step = int(row_width * sizeof(c_uint16))
        for row in range(img_rows):
            offset = row * row_step
            self._libfli.FLIGrabRow(self._dev, byref(img_ptr.contents, offset), row_width)
        return img_array.astype(numpy.int16)


class FakeCamera(object):
    def __init__(self):
        self.temperature = -5
        self.current_binning = 2

    def set_signal_sender(self, signal_sender):
        self.signal_sender = signal_sender

    def set_flushes(self, num):
        pass

    def read_CCD_temperature(self):
        return self.temperature + numpy.random.normal(0, 0.5)

    def read_base_temperature(self):
        return self.temperature + 20 + numpy.random.normal(0, 0.5)

    def get_cooler_power(self):
        return 75 + numpy.random.normal(0, 2)

    def control_background_flushing(self, new_state):
        pass

    def take_photo(self, wait_shutter=False,  camera_status_value=None, emit_signal=False):
        self.exposure_stopped = False
        t_start = time.time()
        if camera_status_value is not None:
            camera_status_value.set("Taking %s exposure" % self.current_frametype)
        for i in range(int(10*self.exptime)):
            delta = time.time() - t_start
            if delta > self.exptime/1000:
                break
            time.sleep(1 / 10)
            if self.exposure_stopped is True:
                print("Exposure aborted")
                camera_status_value.set("Idle")
                return None
        if camera_status_value is not None:
            camera_status_value.set("Fetching image")
        image = self.fetch_image(wait_shutter=wait_shutter, emit_signal=emit_signal)
        if camera_status_value is not None:
            camera_status_value.set("Idle")
        if self.exposure_stopped is True:
            return None
        return image

    def set_exposure(self, exptime, frametype):
        self.exptime = exptime
        self.current_frametype = frametype

    def fetch_image(self, wait_shutter=False, emit_signal=False):
        if emit_signal is True:
            # Send a zmq signal to tell that the shutter is closed
            Thread(target=self.signal_sender.send_signal, args=("dither", )).start()
        time.sleep(1.5)  # Emulate fetching time on 700kHz
        szx = int(1056/self.current_binning)
        szy = int(1026/self.current_binning)
        loc = numpy.random.randint(low=8000, high=12000)
        scale = numpy.random.randint(low=80, high=120)
        img = numpy.random.normal(loc=loc, scale=scale, size=(szx, szy)).astype(dtype=numpy.int16)
        return img.astype(numpy.int16)

    def set_camera_mode(self, mode):
        self.mode = mode

    def get_camera_mode(self):
        return self.mode

    def set_image_binning(self, hbin, vbin):
        self.current_binning = hbin

    def get_temperature(self):
        return self.temperature

    def set_temperature(self, temperature):
        self.temperature = temperature

    def open_shutter(self):
        print("Opening shutter")

    def close_shutter(self):
        print("Closing shutter")

    def cancel_exposure(self):
        pass

    def clean(self, iterations=10):
        self.camera_status.set("Cleaning")
        self.device.exposure_stopped = False
        for i in range(iterations):
            if self.device.exposure_stopped is True:
                break
            self.grab_dark(0.0)
            time.sleep(0.1)
        self.camera_status.set("Idle")


class FLICamera(object):
    def __init__(self, signal_sender):
        self.doing_focus = False
        self.shutter_opened = False
        if os.name == "nt":
            devices_list = USBCamera.find_devices()
            if len(devices_list) == 0:
                raise DeviceIsNotAvailableError("FLI camera is not available")
            self.device = devices_list[0]
        else:
            self.device = FakeCamera()
        # Binning configuration
        self.device.set_signal_sender(signal_sender)
        self.device.set_image_binning(hbin=2, vbin=2)
        self.current_binning = 2
        # Readout frequency configuration
        self.mode_to_idx = {"2MHz": 0, "700kHz": 1}
        self.idx_to_mode = {0: "2MHz", 1: "700kHz"}
        self.device.set_camera_mode(self.mode_to_idx["700kHz"])
        self.current_mode_string = "700kHz"
        # Temperature settings
        self.current_set_temperature = self.device.get_temperature()
        # Turn on the background flushing
        self.device.set_flushes(2)  # Do we really need this?
        self.device.control_background_flushing(True)
        # Status variable
        self.camera_status = None
        # Take a dark exposure to set up the camera
        self.device.set_exposure(exptime=500, frametype="dark")
        self.device.take_photo(wait_shutter=False, camera_status_value=None)
        self.current_expusure_length = 500
        self.current_expusure_type = "dark"

    def set_frequency_mode(self, new_mode_string):
        """
        Set new frequency mode ('700kHz' or '2MHz') if it is not
        selected already
        """
        print("Setting mode to %s" % new_mode_string)
        current_mode_idx = self.device.get_camera_mode()
        current_mode_string = self.idx_to_mode[current_mode_idx]
        if new_mode_string != current_mode_string:
            new_mode_idx = self.mode_to_idx[new_mode_string]
            self.device.set_camera_mode(new_mode_idx)
            self.current_mode_string = new_mode_string

    def set_binning(self, new_binning):
        print("Setting binning to %i" % new_binning)
        if new_binning != self.current_binning:
            self.device.set_image_binning(hbin=new_binning, vbin=new_binning)
            self.current_binning = new_binning

    def set_temperature(self, new_temperature):
        print("Setting temperature to %1.2f" % new_temperature)
        if new_temperature != self.current_set_temperature:
            self.device.set_temperature(new_temperature)
            self.current_set_temperature = new_temperature

    def get_temperatures(self):
        ccd_temperature = self.device.read_CCD_temperature()
        base_temperature = self.device.read_base_temperature()
        cooler_power = self.device.get_cooler_power()
        return ccd_temperature, base_temperature, cooler_power

    def grab_dark(self, dark_exposure_length):
        self.device.set_exposure(exptime=dark_exposure_length, frametype="dark")
        self.current_expusure_length = dark_exposure_length
        self.current_expusure_type = "dark"
        dark_image = self.device.take_photo(wait_shutter=False,
                                            camera_status_value=self.camera_status,
                                            emit_signal=False)
        return dark_image

    def grab_light(self, light_exposure_length, emit_signal=False):
        self.device.set_exposure(exptime=light_exposure_length, frametype="normal")
        self.current_expusure_length = light_exposure_length
        self.current_expusure_type = "normal"
        light_image = self.device.take_photo(wait_shutter=True,
                                             camera_status_value=self.camera_status,
                                             emit_signal=emit_signal)
        return light_image

    def focus_cycle(self, focus_exposure_length, bin_size, image_queue):
        """
        Get a set of open-shutter exposures
        """
        # Remember pre-focusing camera settings so we can revert them
        # when the focusing is finished
        before_fucusing_bin_size = self.current_binning
        before_focusing_freq_mode = self.idx_to_mode[self.device.get_camera_mode()]
        # Setup camera for focusing
        self.set_binning(bin_size)
        self.set_frequency_mode("2MHz")
        self.device.set_exposure(exptime=focus_exposure_length, frametype="normal")
        self.device.open_shutter()
        # Start the focusing cycle
        while 1:
            image = self.device.take_photo(wait_shutter=False, camera_status_value=self.camera_status)
            image_queue.put(image)
            if image is None:
                # Exposition was aborted
                break
        self.device.close_shutter()
        # Flush signal from CCD just in case
        self.clean(iterations=1)
        # Revert pre-focusing camera settings
        self.set_binning(before_fucusing_bin_size)
        self.set_frequency_mode(before_focusing_freq_mode)
        self.clean(iterations=1)

    def clean(self, iterations=10):
        self.camera_status.set("Cleaning")
        self.device.exposure_stopped = False
        for i in range(iterations):
            if self.device.exposure_stopped is True:
                break
            self.grab_dark(0.0)
            time.sleep(0.1)
        self.camera_status.set("Idle")

    def cancel_exposure(self):
        self.device.cancel_exposure()
        self.device.exposure_stopped = True

    def fill_header(self, hdr):
        """
        Function fills some header cards (like temperature, binning etc)
        into the given header
        """
        ccd_temperature = self.device.read_CCD_temperature()
        base_temperature = self.device.read_base_temperature()
        time_string = time.strftime("%Y-%m-%dT%H:%M:%S", time.gmtime())
        hdr["BITPIX"] = 16
        hdr["CCD-TEMP"] = round(ccd_temperature, 2)
        hdr["BASETEMP"] = round(base_temperature, 2)
        hdr["EXPTIME"] = self.current_expusure_length / 1000
        hdr["XBINNING"] = self.current_binning
        hdr["YBINNING"] = self.current_binning
        hdr["INSTRUME"] = 'MicroLine ML4710'
        hdr["CAM-MODE"] = self.current_mode_string
        hdr["DATE"] = time_string
        hdr["DATE-OBS"] = time_string
