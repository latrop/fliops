#! /usr/bin/env python

from os import path
import tkinter as Tk
from tkinter import messagebox
import numpy as np
from scipy import stats
import pylab
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


class AddPointWindow(object):
    def __init__(self, parent, new_t_value, new_focus_value):
        self.new_t_value = new_t_value
        self.new_focus_value = new_focus_value
        self.parent = parent
        self.top = Tk.Toplevel(self.parent.window.root)
        self.top.geometry('+%i+%i' % (self.parent.window.root.winfo_x() + 80,
                                      self.parent.window.root.winfo_y() + 80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.top.protocol('WM_DELETE_WINDOW', self.onclose)
        self.parent.window.popup_exists = True
        # Create a graph
        self.plot_instances = []
        self.mainGraph = pylab.Figure(figsize=(3, 3), dpi=96)
        self.canvas = FigureCanvasTkAgg(self.mainGraph, master=self.top)
        self.fig = self.mainGraph.add_subplot(111)
        self.plot_instances.append(self.fig.plot(self.parent.t_values, self.parent.focus_values, "ro")[0])
        # Perform linear regression of the data
        if len(self.parent.t_values) > 1:
            slope, intercept, *_ = stats.linregress(self.parent.t_values, self.parent.focus_values)
            x1 = min(self.parent.t_values)
            y1 = x1 * slope + intercept
            x2 = max(self.parent.t_values)
            y2 = x2 * slope + intercept
            self.plot_instances.append(self.fig.plot([x1, x2], [y1, y2], color="k"))
        self.plot_instances.append(self.fig.plot([new_t_value], [new_focus_value], "go")[0])
        self.fig.set_xlabel("Temperature", size=14)
        self.fig.set_ylabel("Focus position", size=14)
        self.mainGraph.tight_layout()
        self.canvas.draw()
        self.tk_widget = self.canvas.get_tk_widget()
        self.tk_widget.grid(column=0, row=0, columnspan=2)
        # Create buttons
        self.accept_button = Tk.Button(self.top, text="Accept point", command=self.accept_point)
        self.accept_button.grid(column=0, row=1, pady=8)
        self.discard_button = Tk.Button(self.top, text="Discard point", command=self.onclose)
        self.discard_button.grid(column=1, row=1, pady=8)

    def accept_point(self):
        self.parent.add_point(self.new_t_value, self.new_focus_value)
        self.onclose()

    def onclose(self):
        self.parent.window.popup_exists = False
        self.top.destroy()


class GetTCorrWindow(object):
    def __init__(self, parent, temperature):
        self.parent = parent
        if len(self.parent.t_values) < 2:
            # Not enough points to compute temperature correction
            messagebox.showwarning(parent=self.parent.window.root, title="Warning",
                                   message="Not enough saved points to compute the correction.")
            return
        self.top = Tk.Toplevel(self.parent.window.root)
        self.top.geometry('+%i+%i' % (self.parent.window.root.winfo_x() + 80,
                                      self.parent.window.root.winfo_y() + 80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.top.protocol('WM_DELETE_WINDOW', self.onclose)
        self.parent.window.popup_exists = True
        # Create a graph
        self.plot_instances = []
        self.mainGraph = pylab.Figure(figsize=(3, 3), dpi=96)
        self.canvas = FigureCanvasTkAgg(self.mainGraph, master=self.top)
        self.fig = self.mainGraph.add_subplot(111)
        self.plot_instances.append(self.fig.plot(self.parent.t_values, self.parent.focus_values, "ro")[0])
        # Perform linear regression of the data
        slope, intercept, *_ = stats.linregress(self.parent.t_values, self.parent.focus_values)
        # Show the fit on the plot
        x1 = min(self.parent.t_values)
        y1 = x1 * slope + intercept
        x2 = max(self.parent.t_values)
        y2 = x2 * slope + intercept
        self.plot_instances.append(self.fig.plot([x1, x2], [y1, y2], color="k"))
        # Compute the correction
        self.computed_focus_position = int(temperature * slope + intercept)
        # Show the correction computation on the plot
        xmin, xmax = self.fig.get_xlim()
        ymin, ymax = self.fig.get_ylim()
        self.fig.vlines(x=temperature, ymin=ymin, ymax=self.computed_focus_position, color="0.5", linestyle=":")
        self.fig.hlines(y=self.computed_focus_position, xmin=xmin, xmax=temperature, color="0.5", linestyle=":")
        self.fig.annotate(f"Focus={int(self.computed_focus_position)}",
                          xy=(xmin, self.computed_focus_position+(ymax-ymin)/40),
                          xycoords="data", size=12)
        self.fig.set_xlabel("Temperature")
        self.fig.set_ylabel("Focus")

        self.mainGraph.tight_layout()
        self.canvas.draw()
        self.tk_widget = self.canvas.get_tk_widget()
        self.tk_widget.grid(column=0, row=0, columnspan=2)
        # Add buttons
        self.apply_button = Tk.Button(self.top, text="Apply correction", command=self.apply_focus)
        self.apply_button.grid(column=0, row=1, pady=8)
        self.close_button = Tk.Button(self.top, text="Close", command=self.onclose)
        self.close_button.grid(column=1, row=1, pady=8)

    def apply_focus(self):
        self.parent.window.focuser_rotator.focuser_move_absolute_position(self.computed_focus_position)
        self.onclose()

    def onclose(self):
        self.parent.window.popup_exists = False
        self.top.destroy()


class TCorrWrapper(object):
    def __init__(self, window):
        self.window = window
        self.path_to_data = path.join(path.dirname(__file__), "temp_corr.dat")
        self.t_values = []
        self.focus_values = []
        for line in open(self.path_to_data):
            if line.startswith("#") or (len(line.split()) < 2):
                continue
            self.t_values.append(float(line.split()[0]))
            self.focus_values.append(float(line.split()[1]))

    def show_add_point_dialogue(self, new_t_value, new_focus_value):
        AddPointWindow(self, new_t_value, new_focus_value)

    def add_point(self, new_t_value, new_focus_value):
        self.t_values = np.append(self.t_values, new_t_value)
        self.focus_values = np.append(self.focus_values, new_focus_value)
        np.savetxt(self.path_to_data, np.c_[self.t_values, self.focus_values],
                   header="# temp  focus")

    def show_get_correction_dialogue(self, temperature):
        GetTCorrWindow(self, temperature)

    def reset(self):
        self.t_values = []
        self.focus_values = []
        np.savetxt(self.path_to_data, np.c_[self.t_values, self.focus_values])
