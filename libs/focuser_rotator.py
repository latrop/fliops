#! /usr/bin/env python

import os
import time
import queue
from threading import Thread
from serial import Serial
from serial import SerialException
from .errors import DeviceIsNotAvailableError


class FakeDevice(object):
    def __init__(self):
        self.responses = queue.Queue()
        self.focuser_position = 5000
        self.rotator_position = 0
        self.iReverse = 0
        self.is_focuser_moving = False
        self.is_rotator_moving = False

    def close(self):
        pass

    def move_focuser(self, new_position):
        if self.is_focuser_moving is True:
            return
        if new_position < self.focuser_position:
            step = -1
        else:
            step = 1

        def f():
            self.is_focuser_moving = True
            while self.focuser_position != new_position:
                self.focuser_position += step
                time.sleep(0.003)
            self.is_focuser_moving = False
        Thread(target=f).start()

    def move_rotator(self, new_position):
        if self.is_rotator_moving is True:
            return

        def f():
            if self.rotator_position < 180000:
                old_pa = self.rotator_position
            else:
                old_pa = self.rotator_position - 360000
            if new_position < 180000:
                new_pa = new_position
            else:
                new_pa = new_position - 360000
            if new_pa > old_pa:
                step = +10
            else:
                step = -10
            self.is_rotator_moving = True
            while 1:
                if ((self.rotator_position == new_position) or
                        (self.is_rotator_moving is False) or
                        (self.rotator_position + 360000 == new_position)):
                    break
                # print(self.is_rotator_moving)
                self.rotator_position += step
                time.sleep(0.001)
            self.is_rotator_moving = False
        Thread(target=f).start()

    def halt_rotator(self):
        self.is_rotator_moving = False

    def write(self, command):
        command = command.decode()
        device_name = command[1]
        token = command[3:5]
        command_name = command[5:11]
        payload = command[11:-1]

        # Process the command
        if (device_name == "F") and (command_name == "GETDNN"):
            self.responses.put(f"!{token}\n".encode())
            self.responses.put("Nickname = Focuser\n".encode())
            self.responses.put("END\n".encode())
            self.responses.join()
            return

        if (device_name == "R") and (command_name == "GETDNN"):
            self.responses.put(f"!{token}\n".encode())
            self.responses.put("Nickname = Rotator\n".encode())
            self.responses.put("END\n".encode())
            self.responses.join()
            return

        if (device_name == "F") and (command_name == "GETSTA"):
            self.responses.put(f"!{token}\n".encode())
            self.responses.put("CurrTemp = 0.0\n".encode())
            self.responses.put(f"CurrStep = {int(self.focuser_position)}\n".encode())
            self.responses.put("TargStep = 57600\n".encode())
            if self.is_focuser_moving is True:
                self.responses.put("IsMoving = 1\n".encode())
            else:
                self.responses.put("IsMoving = 0\n".encode())
            self.responses.put("IsHoming = 0\n".encode())
            self.responses.put("Is Homed = 1\n".encode())
            self.responses.put("TempProb = 1\n".encode())
            self.responses.put("END\n".encode())
            self.responses.join()
            return

        if (device_name == "R") and (command_name == "GETSTA"):
            self.responses.put(f"!{token}\n".encode())
            self.responses.put("CurrStep = 45000\n".encode())
            self.responses.put("TargStep = 45000\n".encode())
            self.responses.put(f"CurentPA = {int(self.rotator_position)}\n".encode())
            self.responses.put("TargetPA = 359999\n".encode())
            if self.is_rotator_moving is True:
                self.responses.put("IsMoving = 1\n".encode())
            else:
                self.responses.put("IsMoving = 0\n".encode())
            self.responses.put("IsHoming = 0\n".encode())
            self.responses.put("Is Homed = 1\n".encode())
            self.responses.put("END\n".encode())
            self.responses.join()
            return

        if (device_name == "F") and (command_name == "GETCFG"):
            self.responses.put(f"!{token}\n".encode())
            self.responses.put("Nickname = Castor\n".encode())
            self.responses.put("MaxSteps = 115200\n".encode())
            self.responses.put("Dev Type = A\n".encode())
            self.responses.put("TComp On = 0\n".encode())
            self.responses.put("TCMode A = 86\n".encode())
            self.responses.put("TCMode B = 86\n".encode())
            self.responses.put("TCMode C = 86\n".encode())
            self.responses.put("TCMode D = 86\n".encode())
            self.responses.put("TCMode E = 86\n".encode())
            self.responses.put("CurrenTC = A\n".encode())
            self.responses.put("BLCompOn = 0\n".encode())
            self.responses.put("BLCSteps = 40\n".encode())
            self.responses.put("TC Start = 0\n".encode())
            self.responses.put("HOnStart = 1\n".encode())
            self.responses.put("END\n".encode())
            self.responses.join()
            return

        if (device_name == "R") and (command_name == "GETCFG"):
            self.responses.put(f"!{token}\n".encode())
            self.responses.put("Nickname = Pollux\n".encode())
            self.responses.put("MaxSteps = 215999\n".encode())
            self.responses.put("Dev Type = B\n".encode())
            self.responses.put("BLCompOn = 0\n".encode())
            self.responses.put("BLCSteps = 40\n".encode())
            self.responses.put("PAOffset = 0\n".encode())
            self.responses.put("HonStart = 1\n".encode())
            self.responses.put(f"iReverse = {int(self.iReverse)}\n".encode())
            self.responses.put("MaxSpeed = 800\n".encode())
            self.responses.put("END\n".encode())
            self.responses.join()
            return

        if (device_name == "F") and (command_name == "MOVABS"):
            self.responses.put(f"!{token}\n".encode())
            self.responses.put("END\n".encode())
            self.responses.join()
            self.move_focuser(int(payload))
            return

        if (device_name == "R") and (command_name == "MOVEPA"):
            self.responses.put(f"!{token}\n".encode())
            self.responses.put("END\n".encode())
            self.move_rotator(int(payload))
            self.responses.join()
            return

        if (device_name == "R") and (command_name == "DOHALT"):
            self.responses.put(f"!{token}\n".encode())
            self.responses.put("END\n".encode())
            self.halt_rotator()
            self.responses.join()
            return

        if (device_name == "R") and (command_name == "SETREV"):
            self.iReverse = int(payload)
            self.responses.put(f"!{token}\n".encode())
            self.responses.put("SET\n".encode())
            self.responses.join()
            return

    def readline(self):
        try:
            line = self.responses.get(timeout=0.01)
            self.responses.task_done()
            return line
        except queue.Empty:
            time.sleep(0.01)
            return "".encode()


class GeminiFocuserRotator(object):
    def __init__(self):
        # Connect to the device via the serial port
        if os.name == 'nt':
            try:
                self.device = Serial("COM9", baudrate=115200)
            except SerialException:
                raise DeviceIsNotAvailableError("Focuser-rotator is not available")
        else:
            self.device = FakeDevice()
        self.is_alive = True
        # A pull of error messages. Any error messages will be put here so the main
        # window can take them from this pull and display them
        self.error_messages = queue.Queue()  # TODO: show them on the main window

        # Every command has a token that can be used to retrieve the output of this command. The token
        # is a number from 0 to 99. Let's create a dictionary of 100 queue objects with keys 0 to 99
        # such that threads can put and get responses to and from this dictionary
        self.responses = {"%02i" % i: queue.Queue() for i in range(100)}
        # Create a queue of available tokens. When the command is created, a function that makes this
        # command will take a token from this queue, so that the other function won't use the same token.
        # When the execution of the command is over and the token is free again, it will be put back in this
        # pull again by the response retreaving function.
        self.available_tokens = queue.Queue()
        for i in range(99, -1, -1):
            self.available_tokens.put("%02i" % i)
        # Run response getter
        Thread(target=self.response_getter).start()
        # Load current configuration
        self.focuser_configuration = self.get_focuser_configuration()
        self.rotator_configuration = self.get_rotator_configuration()
        self.rotator_set_reverse_property(False)
        self.rotator_reversed = False
        # Run auto info updateer
        Thread(target=self.update_parameters).start()

    def update_parameters(self):
        # Load parameters of the devices
        while self.is_alive:
            self.focuser_status = self.get_focuser_status()
            self.rotator_status = self.get_rotator_status()
            # print(self.rotator_status["CurentPA"], self.rotator_status["TargetPA"])
            time.sleep(0.75)

    def shutdown(self):
        """ Shut down focuser-rotator device """
        # Before shutting down, set it to a zero-point position
        def f():
            self.rotator_move_absolute_position(0)
            while self.rotator_status["CurentPA"] != 0:
                time.sleep(0.1)
            self.is_alive = False
            self.device.close()
        Thread(target=f).start()

    def send_command(self, command):
        # Get a token from a command
        # print("sending command", command)
        token = command[3:5]
        # Send command
        self.device.write(str.encode(command, "utf8"))
        # Wait for response to appear in the corresponging gueue
        response = self.responses[token].get()
        # return token back into the pool
        self.available_tokens.put(token)
        # Check if the response contains an error message
        if response.startswith("ERROR"):
            # Yep, we have an error here. Let's parse it and see what it is all about
            error_id = int(response.split("\n")[0].split("=")[1])
            error_text = response.split("\n")[1].split("=")[1]
            self.error_messages.put((error_id, error_text))
            return None
        return response

    def response_getter(self):
        """
        This function hangs in a separate thread, constantly reading the device output,
        parsing the responses and dispatching them in the reponces dictionary
        """
        response = ""
        while self.is_alive:
            try:
                new_line = self.device.readline()
            except SerialException:
                print("Could not read from COM9")
                continue
            try:
                new_line = new_line.decode("utf8")
            except TypeError:
                print("Could not convert the string from COM9")
                continue
            if len(new_line) == 0:
                response = ""
                continue
            if new_line.startswith("!"):
                # This is the beginning of a new response
                response = ""
                token = new_line[1:3]
                # print(f"Getting a new response with token {token}")
                continue
            if (new_line.strip() == "END") or (new_line.strip() == "SET"):
                # This is the end of the responce. Put it into the
                self.responses[token].put(response)
                continue
            # We are in the middle of the response
            response += new_line

    # Below there is a list of functions to represend the focuser-rotator commands
    def get_focuser_nickname(self):
        """
        Description: This command requests the focuser identification string or “Nickname”. Valid string
        lengths for “Nickname” are 1 to 16 ASCII characters. The controller will respond with the focuser’s user-
        defined nickname.
        Note: GETDNN is generally used to ping the device to check communication status.
        """
        token = self.available_tokens.get()
        command = f"<F1{token}GETDNN>"
        response = self.send_command(command)
        if response is None:
            return ""
        nickname = response.split("=")[1].strip()
        return nickname

    def get_rotator_nickname(self):
        """
        Description: This command requests the rotator identification string or “Nickname”. Valid string
        lengths for “Nickname” are 1 to 16 ASCII characters. The controller will respond with the rotator’s user-
        defined nickname.
        Note: GETDNN is generally used to ping the device to check communication status.
        """
        token = self.available_tokens.get()
        command = f"<R1{token}GETDNN>"
        response = self.send_command(command)
        if response is None:
            return ""
        nickname = response.split("=")[1].strip()
        return nickname

    def get_focuser_status(self):
        """
        Description: This command will request the controller to report the status of the specified focuser. The
        first line of the status report is “!ii” and the last line is “END” to indicate that the output is complete. The
        status output contains the following fields:
        CurrTemp:
            The current temperature in degrees Celsius.
        CurrStep:
            The current step position of the specified focuser.
        TargStep:
            The target step position the focuser is currently moving toward.
        IsMoving:
            Boolean flag is set to ‘1’ if the device is moving, ‘0’ if the device is stationary.
        IsHoming:
            Boolean flag is set to ‘1’ while the device is homing and ‘0’ otherwise.
        Is Homed:
            Boolean flag is set to ‘1’ if the focuser has been homed, ‘0’ if not yet homed.
        TempProb:
            Boolean flag indicates the status of an attached temperature probe. A value of
        ‘1’ means a probe is attached, ‘0’ means no probe is detected.
        The status will be returned as a dictionary containing these values.

        Note: CurrTemp, CurrStep, and TargStep are each reported as numerical values with no additional
        padding. Additionally, the CurrTemp reponse includes a sign and decimal point in the output ascii string.
        """
        token = self.available_tokens.get()
        command = f"<F1{token}GETSTA>"
        response = self.send_command(command)
        if response is None:
            return None
        status = {}
        for line in response.split("\n"):
            if '=' in line:
                parameter_name = line.split('=')[0].strip()
                try:
                    parameter_value = float(line.split('=')[1])
                except ValueError:
                    parameter_value = line.split('=')[1].strip()
                parameter_value = float(line.split('=')[1])
                status[parameter_name] = parameter_value
        return status

    def get_rotator_status(self):
        """
        Description: This command will request the controller to report the status of the specified rotator.
        The first line of the status report is “!ii” and the last line is “END” to indicate that the output is complete.
        The status output contains the following fields:
        CurrStep:
            The current step position of the specified rotator.
        TargStep:
            The target step position that the rotator is currently moving toward.
        CurentPA:
            The current position angle (x1000) for the specified rotator.
        TargetPA:
            The target position angle (x1000) the rotator is moving toward.
        IsMoving:
            Boolean flag is set to ‘1’ if the device is moving and ‘0’ if the device is stationary.
        IsHoming:
            Boolean flag is set ‘1’ while the device is homing and ‘0’ otherwise.
        Is Homed:
            Boolean flag will be set to ‘1’ after the focuser is home, ‘0’ otherwise.
        The status will be returned as a dictionary containing these values.

        Note: CurrStep, TargStep, CurentPA, and TargetPA are each reported as ascii strings representing
        numerical values with no additional padding.
        """
        token = self.available_tokens.get()
        command = f"<R1{token}GETSTA>"
        response = self.send_command(command)
        if response is None:
            return None
        status = {}
        for line in response.split("\n"):
            if '=' in line:
                parameter_name = line.split('=')[0].strip()
                try:
                    parameter_value = float(line.split('=')[1])
                except ValueError:
                    parameter_value = line.split('=')[1].strip()
                status[parameter_name] = parameter_value
        return status

    def get_focuser_configuration(self):
        """
        Description: This command will request the controller to report the configuration settings of the
        specified focuser. The first line of the configuration report is “!ii” and the last line is “END” to indicate
        that the output is complete. The configuration output contains the following fields:
        Nickname:
            The user-defined nickname of the specified focuser.
        MaxSteps:
            The maximum absolute position, MAXPOS, for the selected focuser.
        Dev Type:
            A one character designator for the currently set device type, see Appendix A.
        TComp On:
            Boolean value, ‘1’ = true (temp comp on) and ‘0’ = false (temp comp off).
        TCMode A (through TCModeCurrentTC:
            Temperature Coefficient (TC) value for each of five available settings, A to E.)
            Note: All TC value units are negative steps per degree C.
        BLCompOn:
            Indicates the currently active temperature compensation mode, ‘A’ through ‘E’.
        BLCSteps:
            BackLash Compensation On, ‘1’ when true, ‘0’ when compensation is disabled.
            Number of steps set in EEPROM for backlash compensation.
            Note: the focuser will travel past the target position before returning to the
            target position in order to compensate for mechanical backlash in the focusing
            device. Backlash compensation applies to IN direction moves only.
            Indicates Temperature Compensation is active, ‘1’ means on, ‘0’ means off.
            Home on Start- Boolean value, ‘1’ is true, ‘0’ means do not home on start.
        """
        token = self.available_tokens.get()
        command = f"<F1{token}GETCFG>"
        response = self.send_command(command)
        if response is None:
            return None
        settings = {}
        for line in response.split("\n"):
            if '=' in line:
                parameter_name = line.split('=')[0].strip()
                try:
                    parameter_value = float(line.split('=')[1])
                except ValueError:
                    parameter_value = line.split('=')[1].strip()
                settings[parameter_name] = parameter_value
        return settings

    def get_rotator_configuration(self):
        """
        Description: This command will request the controller to report the configuration settings of the
        specified rotator. The first line of the configuration report is “!ii” and the last line is “END” to indicate
        that the output is complete. The configuration output contains the following fields:
        Nickname:
            The user-defined nickname of the specified rotator.
        MaxSteps:
            The maximum step position the selected rotator is capable of moving to.
        Dev Type:
            A one character designator for the currently set device type, see Appendix A.
        BLCompOn:
            Boolean flag indicates whether backlash compensation is active.
            A value of ‘1’ indicates rotator will perform backlash compensation, ‘0’ indicates
            the feature is turned off.
        BLCSteps:
            This numeric string value indicates the number of steps that the rotator will
            travel past the target position before returning to the target position in order to
            compensate for mechanical backlash in the rotating device.
        PAOffset:
            The Position Angle Offset is a numeric string value that indicates the number of
            steps required to adjust the instrumental Position Angle (PA) of PA=0 to match
            Sky PA=0. This is a residual setting from early Pyxis rotator implementations.
        HonStart:
            The Home on Start Boolean indicates if the rotator will home upon initial
            power-up or start. A value of ‘1’ indicates the rotator will perform a home at
            start, ‘0’ indicates the focuser will not home at start.
        iReverse:
            Reverse Boolean indicates whether the reverse flag is set to true (value = ‘1’)
            or false (value = ‘0’).
        MaxSpeed:
            MaxSpeed is the maximum speed in steps per second for the rotator motor.
            Note: This command is reserved for possible future implementation allowing
            client software setting of the rotator speed.
        """
        token = self.available_tokens.get()
        command = f"<R1{token}GETCFG>"
        response = self.send_command(command)
        if response is None:
            return None
        settings = {}
        for line in response.split("\n"):
            if '=' in line:
                parameter_name = line.split('=')[0].strip()
                try:
                    parameter_value = float(line.split('=')[1])
                except ValueError:
                    parameter_value = line.split('=')[1].strip()
                settings[parameter_name] = parameter_value
        return settings

    def focuser_halt(self):
        """
        Description: This command immediately halts the current focuser movement. The controller will
        respond immediately with the “!ii” and “END” strings to acknowledge the command was received.
        Note: If Temperature Compensation was active at the time the Halt command was received,
        temperature compensation will be automatically disabled.
        """
        token = self.available_tokens.get()
        command = f"<F1{token}DOHALT>"
        response = self.send_command(command)
        if response == "":
            return 0
        else:
            return 1

    def rotator_halt(self):
        """
        Description: This command immediately halts the current rotator movement. The controller will
        respond with the “!ii” and “END” strings to acknowledge the command was received.
        Note: If the rotator HALT is performed while the magnet is directly over the hall sensor, the IsHomed
        property will be set to false and a HOME procedure will be required.
        """
        token = self.available_tokens.get()
        command = f"<R1{token}DOHALT>"
        response = self.send_command(command)
        if response == "":
            return 0
        else:
            return 1

    def focuser_move_absolute_position(self, new_position):
        """
        Description: This command requests the specified focuser to begin moving to the absolute position
        specified. The target position must be between 0 and the focuser’s maximum position. The Target
        Position will be set to z and the IsMoving flag will be set to True (‘1’) until the completion of the action.
        Note: The ‘z’ payload parameter can be padded to six characters if preferred, but padding is not
        required. (i.e. ‘z’ = 100 is equivalent to ‘z’ = 000100.)
        """
        # Make sure we have an integer value
        try:
            new_position = int(new_position)
        except ValueError:
            return 2
        # Make sure that the new position is inside the allowed range
        if not (0 <= new_position <= self.focuser_configuration["MaxSteps"]):
            print(0, new_position, self.focuser_configuration["MaxSteps"])
            return 2
        token = self.available_tokens.get()
        command = f"<F1{token}MOVABS{new_position}>"
        response = self.send_command(command)
        if response == "":
            return 0
        else:
            return 1

    def rotator_set_reverse_property(self, new_reverse_value):
        """
        Description: The Set Reverse Property command allows the client to set the rotator reverse property to
        true or false. A payload parameter y=’1’ sets Reverse to true; y=’0’ sets Reverse to false meaning the
        direction of movement will be in the default direction. The default direction for increasing Position
        Angle values is always clockwise. Setting the Reverse property to true will not affect the underlying step
        position of the rotator. However, the Gemini hub responses for position angles will be reversed when
        this property is true. Instrumental PA=0 and PA=180 will be unaffected.
        Note: The Gemini rotator will always home in the anti-clockwise direction until it locates the home
        magnet, then move in a clockwise direction to instrumental PA = 0. From that position
        """
        if new_reverse_value is True:
            y = 1
        else:
            y = 0
        if self.rotator_configuration['iReverse'] == y:
            # We already have the demanded property set. Do nothing.
            return 0
        token = self.available_tokens.get()
        command = f"<R1{token}SETREV{y}>"
        response = self.send_command(command)
        if response is not None:
            # Setting successfully set
            self.rotator_configuration['iReverse'] = y
            self.rotator_reversed = new_reverse_value
            return 0
        else:
            return 1

    def rotator_move_absolute_position(self, new_position):
        """
        Description: This command requests the specified rotator to begin moving to the absolute position
        angle specified. The target position must be between -359999 and the 359999. The TargetPA will be set to ‘z’
        and IsMoving will be set to true (‘1’). If the TargetPA is negative than the iReverse option will be
        enabled.
        Note: All position angles are instrumental position angles and multiplied by 1000 with no decimal point.
        (i.e. instrumental PA=15 is represented by 15000).
        """
        try:
            new_position = int(new_position)
        except ValueError:
            return 2
        # Make sure that the new position is inside the allowed range
        if not (-359999 <= new_position <= 359999):
            return 2
        if new_position < 0:
            # We need to rotate in a CW direction
            new_position += 360000
        token = self.available_tokens.get()
        command = f"<R1{token}MOVEPA{abs(new_position)}>"
        response = self.send_command(command)
        if response == "":
            return 0
        else:
            return 1
