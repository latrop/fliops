#! /usr/bin/env python

import time
import tkinter as Tk
from tkinter import messagebox
from os import path
from threading import Thread
from tkinter import filedialog
from queue import Queue
from queue import Empty
from astropy.io import fits

from .. import honk


class FuncPanel(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.panel = Tk.Frame(self.window.root)
        self.panel.grid(column=0, row=0, sticky=Tk.N)

        # Create buttons
        # Setup button
        self.setup_button = Tk.Button(self.panel, text="Setup", font=("bold"),
                                      command=self.show_setup_window)
        self.setup_button.grid(column=0, row=0)

        # Take bias button
        self.bias_button = Tk.Button(self.panel, text="Bias", font=("bold"),
                                     command=self.show_bias_window)
        self.bias_button.grid(column=1, row=0)

        # Take dark button
        self.dark_button = Tk.Button(self.panel, text="Dark", font=("bold"),
                                     command=self.show_grab_dark_window)
        self.dark_button.grid(column=2, row=0)

        # Focus button
        self.focus_button = Tk.Button(self.panel, text="Focus", font=("bold"),
                                      command=self.show_focus_window)
        self.focus_button.grid(column=3, row=0)

        # Clean button
        # self.clean_button = Tk.Button(self.panel, text="Clean", font=("bold"),
        #                               command=self.clean)
        # self.clean_button.grid(column=4, row=0)

        # Next button
        self.next_button = Tk.Button(self.panel, text="Next", font=("bold"),
                                     command=self.set_next, state='disabled')
        self.next_button.grid(column=4, row=0)

        # 'Save as' button to save last taken frame
        self.last_data_and_header = None
        self.save_as_button = Tk.Button(self.panel, text="Save as", font=("bold"),
                                        command=self.save_as)
        self.save_as_button.grid(column=5, row=0)


    def lock(self):
        self.setup_button.config(state='disabled')
        self.bias_button.config(state='disabled')
        self.dark_button.config(state='disabled')
        self.focus_button.config(state='disabled')
        self.next_button.config(state='normal')
        self.save_as_button.config(state='disabled')

    def unlock(self):
        self.setup_button.config(state='normal')
        self.bias_button.config(state='normal')
        self.dark_button.config(state='normal')
        self.focus_button.config(state='normal')
        self.next_button.config(state='disabled')
        self.save_as_button.config(state='normal')

    def show_setup_window(self):
        if not self.window.popup_exists:
            SetupWindow(self.window)

    def show_bias_window(self):
        if not self.window.popup_exists:
            GrabBiasWindow(self.window)

    def show_grab_dark_window(self):
        if not self.window.popup_exists:
            GrabDarkWindow(self.window)

    def show_focus_window(self):
        if not self.window.popup_exists:
            FocusWindow(self.window)

    # def clean(self):
    #     def f():
    #         self.lock()
    #         self.window.grabPanel.grab_lock()
    #         self.window.flicamera.clean()
    #         self.window.grabPanel.grab_unlock()
    #         self.unlock()
    #     Thread(target=f).start()

    def set_next(self):
        if not self.window.popup_exists:
            SetNextWindow(self.window)

    def save_as(self):
        if self.window.popup_exists:
            return
        self.window.popup_exists = True
        if self.last_data_and_header is None:
            messagebox.showwarning(parent=self.window.root, title="Warning",
                                   message="Take an image first!")
            self.window.popup_exists = False
            return False
        file_name = filedialog.asksaveasfilename(parent=self.window.root,
                                                 defaultextension="*.FIT",
                                                 confirmoverwrite=True,
                                                 initialdir=self.window.path_to_save_files,
                                                 filetypes=[["All", "*"]])
        if file_name:
            hdu = fits.PrimaryHDU(data=self.last_data_and_header[0], header=self.last_data_and_header[1])
            hdu.header["BSCALE"] = 1
            hdu.header["BZERO"] = 0
            hdu.writeto(file_name, overwrite=True)
        self.window.popup_exists = False


class SetupWindow(Tk.Frame):
    """
    A window with some preferences that appears when "Setup" button is pressed
    """
    def __init__(self, window):
        self.window = window
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("Setup")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x() + 80,
                                      self.window.root.winfo_y() + 80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True
        self.top.protocol('WM_DELETE_WINDOW', self.onclose)

        # Directory
        self.working_directory_value = Tk.StringVar()
        self.working_directory_value.set(self.window.path_to_save_files)
        Tk.Label(self.top, text="Directory:").grid(column=0, row=0, sticky=Tk.W, padx=5, pady=5)
        Tk.Entry(self.top, textvariable=self.working_directory_value).grid(column=0, row=1, padx=5)
        Tk.Button(self.top, text="Choose", command=self.select_directory).grid(column=1, row=1, padx=5, sticky=Tk.W)

        # Cooler temperature
        Tk.Label(self.top, text="CCD temperature").grid(column=0, row=2, padx=10, pady=5, sticky=Tk.E)
        self.ccd_temperature_value = Tk.StringVar()
        self.ccd_temperature_value.set(round(self.window.flicamera.current_set_temperature))
        Tk.Entry(self.top, textvariable=self.ccd_temperature_value, width=5).grid(column=1, row=2, padx=10,
                                                                                  sticky=Tk.W, pady=5)

        # Readout frequency
        Tk.Label(self.top, text="Readout freq.").grid(column=0, row=3, padx=10, pady=5, sticky=Tk.E)
        freq_options = ["2MHz", "700kHz"]
        self.frequency_value = Tk.StringVar()
        self.frequency_value.set(freq_options[self.window.flicamera.device.get_camera_mode()])
        self.freq_menu = Tk.OptionMenu(self.top, self.frequency_value, *freq_options)
        self.freq_menu.config(width=6)
        self.freq_menu.grid(column=1, row=3, pady=5, padx=5, sticky=Tk.W)

        # Bin size radiobutton
        Tk.Label(self.top, text="Bin size").grid(column=0, row=4, rowspan=2, padx=10, pady=5, sticky=Tk.E)
        self.bin_size_value = Tk.IntVar()
        Tk.Radiobutton(self.top, text="1", variable=self.bin_size_value, value=1).grid(column=1, row=4, sticky=Tk.W)
        Tk.Radiobutton(self.top, text="2", variable=self.bin_size_value, value=2).grid(column=1, row=5, sticky=Tk.W)
        self.bin_size_value.set(self.window.flicamera.current_binning)

        # Alarm checkbox
        Tk.Label(self.top, text="Alarm").grid(column=0, row=6, padx=10, pady=5, sticky=Tk.E)
        self.alarm_value = Tk.IntVar()
        if self.window.alarm_active is True:
            self.alarm_value.set(1)
        else:
            self.alarm_value.set(0)
        Tk.Checkbutton(self.top, variable=self.alarm_value).grid(column=1, row=6, padx=5,
                                                                 pady=5, sticky=Tk.W)

        # Delay entry
        Tk.Label(self.top, text="Delay (sec)").grid(column=0, row=7, padx=10, pady=5, sticky=Tk.E)
        self.exposure_delay_value = Tk.StringVar()
        self.exposure_delay_value.set(str(self.window.exposure_delay))
        entry = Tk.Entry(self.top, textvariable=self.exposure_delay_value, width=5)
        entry.grid(column=1, row=7, padx=5, pady=5, sticky=Tk.W)

        # Rotator zero point
        Tk.Label(self.top, text="Rotator offset").grid(column=0, row=8, padx=10, pady=5, sticky=Tk.E)
        self.rotator_zero_point_value = Tk.StringVar()
        self.rotator_zero_point_value.set(self.window.config_params.params["rotator_zero_point"])
        entry = Tk.Entry(self.top, textvariable=self.rotator_zero_point_value, width=5, state="normal")
        entry.grid(column=1, row=8, padx=5, pady=5, sticky=Tk.W)

        # Reset focuser temperature correction data
        Tk.Label(self.top, text="Temp. correction:").grid(column=0, row=9, padx=10, pady=5, sticky=Tk.E)
        self.reset_tcorr_button = Tk.Button(self.top, text="Reset", command=self.reset_tcorr)
        self.reset_tcorr_button.grid(column=1, row=9, pady=10)

        # Apply button
        Tk.Button(self.top, text="Apply", command=self.apply_changes).grid(column=0, row=10, pady=10)

        # Cancel button
        Tk.Button(self.top, text="Cancel", command=self.onclose).grid(column=1, row=10, pady=10)

        # Override destroy method
        self.top.protocol("WM_DELETE_WINDOW", self.onclose)

    def select_directory(self):
        new_directory = filedialog.askdirectory(parent=self.top,
                                                initialdir=self.working_directory_value.get())
        if new_directory:
            self.working_directory_value.set(new_directory)

    def apply_changes(self):
        # Validate values before applying changes
        validate_result = self.validate()
        if validate_result is False:
            return
        # Set new path to working directory
        self.window.path_to_save_files = self.working_directory_value.get()
        # Set cooler temperature
        self.window.flicamera.set_temperature(round(float(self.ccd_temperature_value.get())))
        # Set frequency mode
        self.window.flicamera.set_frequency_mode(self.frequency_value.get())
        # Set binning
        self.window.flicamera.set_binning(self.bin_size_value.get())
        # Set alarm
        if self.alarm_value.get() == 1:
            self.window.alarm_active = True
        else:
            self.window.alarm_active = False
        # Set delay
        self.window.exposure_delay = float(self.exposure_delay_value.get())
        # Set rotator zero point
        self.window.config_params.update_parameter("rotator_zero_point", float(self.rotator_zero_point_value.get()))
        self.onclose()

    def validate(self):
        """
        Validate entry values before applying changes
        """
        # Validate CCD temperature settings
        try:
            float(self.ccd_temperature_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Wrong CCD temperature value!")
            return False
        # Validate exposure delay settings
        try:
            float(self.exposure_delay_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Wrong exposure delay value!")
            return False
        if float(self.exposure_delay_value.get()) < 0.0:
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Exposure delay should be non-negative.")
            return False
        # Validate rotator zero point value
        try:
            float(self.rotator_zero_point_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Wrong rotator zero point value!")
            return False
        if not (-180 <= float(self.rotator_zero_point_value.get()) <= 180):
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Rotator zero point value should be between -180 and 180!")
            return False
        return True

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()

    def reset_tcorr(self):
        answer = messagebox.askokcancel("Reset", "Clear all temperature corrections data?",
                                        parent=self.top)
        if answer is True:
            self.window.tcorr_wrapper.reset()


class GrabBiasWindow(Tk.Frame):
    """
    Grab bias frames window
    """
    def __init__(self, window):
        self.window = window
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("   Grab bias")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x()+80,
                                      self.window.root.winfo_y()+80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True

        # Bias name entry
        Tk.Label(self.top, text="   Bias name:").grid(column=0, row=0, padx=10)
        self.bias_name_value = Tk.StringVar()
        self.bias_name_value.set("bias")
        Tk.Entry(self.top, textvariable=self.bias_name_value, width=7).grid(column=1, row=0, padx=15)

        # Bias count entry
        Tk.Label(self.top, text="Count:").grid(column=0, row=1, padx=10)
        self.bias_count_value = Tk.StringVar()
        self.bias_count_value.set(str(5))
        Tk.Entry(self.top, textvariable=self.bias_count_value, width=7).grid(column=1, row=1)

        # Start button
        Tk.Button(self.top, text="Start", command=self.start).grid(column=0, row=2, pady=10)

        # Cancel button
        Tk.Button(self.top, text="Cancel", command=self.onclose).grid(column=1, row=2, pady=10)

        # Override destroy method
        self.top.protocol("WM_DELETE_WINDOW", self.onclose)

    def validate(self):
        try:
            count = int(self.bias_count_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong count value!")
            return False
        if count <= 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Count must be positive!")
            return False
        return True

    def start(self):
        validate_result = self.validate()
        if validate_result is False:
            return
        Thread(target=self.grab_biases).start()

    def grab_biases(self):
        # Get values from entries
        bias_common_name = self.bias_name_value.get()
        num_of_biases = int(self.bias_count_value.get())

        # Let's check at first, if we are goig to overwrite existing files
        for idx in range(0, num_of_biases):
            out_name = path.join(self.window.path_to_save_files, "%sS%i.FIT" % (bias_common_name, idx))
            if path.exists(out_name):
                answer = messagebox.askokcancel("Files exist already", "Overwrite existing files?")
                if answer is False:
                    # The user has decided not to ovewrite files. Return to the grab control window
                    return
                else:
                    # The user has decided to overwite existing files. Proceed with the function
                    break

        # We are going to grab darks now, so the control window is not needed anymore
        self.onclose()

        # Lock func and grab panels
        Thread(target=self.window.funcPanel.lock).start()
        Thread(target=self.window.grabPanel.set_grabbing_mode).start()

        # Grab darks
        self.window.grabPanel.desired_exposures_value.set(num_of_biases)
        for idx in range(0, num_of_biases):
            self.window.grabPanel.current_expusure_number_value.set(idx+1)
            # Bias has zero exposure, but let's set some shirt time duration for
            # progress bar, so it blinks real quick and user is happy with the animation
            self.window.grabPanel.run_progress_bar_in_background(0.5 / 1000.0)  # 0.5 sec
            # Shot dark frame and retrieve it from camera
            bias_image = self.window.flicamera.grab_dark(0.0)
            if bias_image is None:
                # Exposure was stopped
                break
            # Fill header
            header = fits.header.Header()
            self.window.flicamera.fill_header(header)
            header["IMAGETYP"] = 'Bias Frame'
            # Save file
            out_name = path.join(self.window.path_to_save_files, "%sS%i.FIT" % (bias_common_name, idx))
            hdu = fits.PrimaryHDU(data=bias_image, header=header)
            hdu.header["BSCALE"] = 1
            hdu.header["BZERO"] = 0
            hdu.writeto(out_name, overwrite=True)
            # Show image
            self.window.imagPanel.image_queue.put((bias_image, path.basename(out_name)))
            # Store the image so it can be manually saved
            self.window.funcPanel.last_data_and_header = (bias_image, header)
            time.sleep(1.0)
        # Remove the progress information from the grab panel
        self.window.grabPanel.current_expusure_number_value.set("-")
        self.window.grabPanel.desired_exposures_value.set("-")
        # Unlock func and grab panels
        self.window.funcPanel.unlock()
        self.window.grabPanel.set_idle_mode()

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()


class GrabDarkWindow(Tk.Frame):
    """
    Grab dark frames window
    """
    def __init__(self, window):
        self.window = window
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("   Grab dark")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x()+80,
                                      self.window.root.winfo_y()+80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True

        # Dark name entry
        Tk.Label(self.top, text="   Dark name:").grid(column=0, row=0, padx=10)
        self.dark_name_value = Tk.StringVar()
        self.dark_name_value.set("dark%i" % (self.window.dark_number+1))
        Tk.Entry(self.top, textvariable=self.dark_name_value, width=7).grid(column=1, row=0, padx=15)

        # Dark exposure entry
        Tk.Label(self.top, text="  Exposure:").grid(column=0, row=1, padx=10)
        self.dark_exposure_length_value = Tk.StringVar()
        self.dark_exposure_length_value.set(60)
        Tk.Entry(self.top, textvariable=self.dark_exposure_length_value, width=7).grid(column=1, row=1)

        # Dark count entry
        Tk.Label(self.top, text="Count:").grid(column=0, row=2, padx=10)
        self.dark_count_value = Tk.StringVar()
        self.dark_count_value.set(str(5))
        Tk.Entry(self.top, textvariable=self.dark_count_value, width=7).grid(column=1, row=2)

        # Start button
        Tk.Button(self.top, text="Start", command=self.start).grid(column=0, row=3, pady=10)

        # Cancel button
        Tk.Button(self.top, text="Cancel", command=self.onclose).grid(column=1, row=3, pady=10)

        # Override destroy method
        self.top.protocol("WM_DELETE_WINDOW", self.onclose)

    def validate(self):
        try:
            exposure = float(self.dark_exposure_length_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong exposure length value!")
            return False
        if exposure < 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Exposure length must be positive!")
            return False
        try:
            count = int(self.dark_count_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong count value!")
            return False
        if count <= 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Count value must be positive!")
            return False
        return True

    def start(self):
        validate_result = self.validate()
        if validate_result is False:
            return
        Thread(target=self.grab_darks).start()

    def grab_darks(self):
        # Get the values from the entries
        num_of_darks = int(self.dark_count_value.get())
        dark_exposure_length = float(self.dark_exposure_length_value.get())
        dark_common_name = self.dark_name_value.get()

        # Let's check at first, if we are goig to overwrite existing files
        for idx in range(0, num_of_darks):
            out_name = path.join(self.window.path_to_save_files, "%sS%i.FIT" % (dark_common_name, idx))
            if path.exists(out_name):
                answer = messagebox.askokcancel("Files exist already", "Overwrite existing files?")
                if answer is False:
                    # The user has decided not to ovewrite files. Return to the grab control window
                    return
                else:
                    # The user has decided to overwite existing files. Proceed with the function
                    break

        # We are going to grab darks now, so the control window is not needed anymore
        self.onclose()

        # Lock func and grab panels
        Thread(target=self.window.funcPanel.lock).start()
        Thread(target=self.window.grabPanel.set_grabbing_mode).start()

        # Grab darks
        self.window.grabPanel.desired_exposures_value.set(num_of_darks)
        for idx in range(0, num_of_darks):
            self.window.grabPanel.current_expusure_number_value.set(idx + 1)
            self.window.grabPanel.run_progress_bar_in_background(dark_exposure_length)
            self.window.grabPanel.run_timer_in_background(dark_exposure_length)

            # Shot dark frame and retrieve it from camera
            dark_image = self.window.flicamera.grab_dark(1000 * dark_exposure_length)
            if dark_image is None:
                # Exposure was stopped
                break
            # Fill header
            header = fits.header.Header()
            self.window.flicamera.fill_header(header)
            header["IMAGETYP"] = 'Dark Frame'
            # Save file
            out_name = path.join(self.window.path_to_save_files, "%sS%i.FIT" % (dark_common_name, idx))
            hdu = fits.PrimaryHDU(data=dark_image, header=header)
            hdu.header["BSCALE"] = 1
            hdu.header["BZERO"] = 0
            hdu.writeto(out_name, overwrite=True)
            # Show image
            self.window.imagPanel.image_queue.put((dark_image, path.basename(out_name)))
            # Store the image so it can be manually saved
            self.window.funcPanel.last_data_and_header = (dark_image, header)
            if self.window.alarm_active and (idx == (num_of_darks - 2)):
                # Second to last exposure just finished
                honk.honk(500, 500)
            elif self.window.alarm_active and (idx == (num_of_darks - 1)):
                # Last exposure just finished
                honk.honk(750, 500)

        else:
            # Extract dark name number from the name given to use it next time to
            # create a new name. This will be executed only if the exposition was not aborted
            self.window.dark_number = int("".join([c for c in dark_common_name if c.isnumeric()]))
        # Set status to idling
        # Remove the progress information from the grab panel
        self.window.grabPanel.current_expusure_number_value.set("-")
        self.window.grabPanel.desired_exposures_value.set("-")
        # Unlock func and grab panels
        self.window.funcPanel.unlock()
        self.window.grabPanel.set_idle_mode()

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()


class FocusWindow(Tk.Frame):
    """
    The window for configuring focus parameters
    """
    def __init__(self, window):
        self.window = window
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("Focus")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x()+80,
                                      self.window.root.winfo_y()+80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True

        # Exposure entry
        Tk.Label(self.top, text="Exposure: ").grid(column=0, row=0, padx=10, pady=10)
        self.exposure_value = Tk.StringVar()
        self.exposure_value.set(2)
        self.exposure_entry = Tk.Entry(self.top, textvariable=self.exposure_value, width=6)
        self.exposure_entry.grid(column=1, row=0, padx=15, columnspan=2)
        self.exposure_entry.bind("<Return>", lambda e: self.start())
        self.exposure_entry.focus()

        # Bin size radiobuttons
        Tk.Label(self.top, text="Bin size").grid(column=0, row=1, padx=10, pady=10, sticky=Tk.E)
        self.bin_size_value = Tk.IntVar()
        self.bin_size_value.set(self.window.flicamera.current_binning)
        Tk.Radiobutton(self.top, text="1", variable=self.bin_size_value, value=1).grid(column=1, row=1, sticky=Tk.W)
        Tk.Radiobutton(self.top, text="2", variable=self.bin_size_value, value=2).grid(column=2, row=1, sticky=Tk.W)

        # Start button
        Tk.Button(self.top, text="Start", command=self.start).grid(column=0, row=2, pady=10)

        # Cancel button
        Tk.Button(self.top, text="Cancel", command=self.onclose).grid(column=1, row=2, pady=10, columnspan=2)

        # Override destroy method
        self.top.protocol("WM_DELETE_WINDOW", self.onclose)

    def validate(self):
        try:
            exposure = float(self.exposure_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong exposure value!")
            return False
        if exposure <= 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Exposure length must be positive!")
            return False
        return True

    def start(self):
        validate_result = self.validate()
        if validate_result is False:
            return
        self.onclose()
        bin_size = int(self.bin_size_value.get())
        exposure_length = float(self.exposure_value.get())
        image_queue = Queue()
        # Lock func and grab panels
        Thread(target=self.window.funcPanel.lock).start()
        Thread(target=self.window.grabPanel.set_grabbing_mode).start()
        # Start focusing mode on the camera side
        Thread(target=self.window.flicamera.focus_cycle,
               args=(1000*exposure_length, bin_size, image_queue)).start()
        # Start focus processing
        Thread(target=self.process_focusing, args=(exposure_length, image_queue)).start()

    def process_focusing(self, exposure_length, image_queue):
        timeout = max(5, 2 * exposure_length)
        self.window.grabPanel.run_progress_bar_in_background(exposure_length+1)
        self.window.grabPanel.run_timer_in_background(exposure_length)
        idx = 0
        while 1:
            idx += 1
            # Wait for a new image, but not longer that for
            try:
                focus_image = image_queue.get(timeout=timeout)
            except Empty:
                # The camera didn't provide a new image for far too long
                break
            if focus_image is None:
                # Focusing aborted
                break
            # Run progress bar for the next image
            self.window.grabPanel.run_progress_bar_in_background(exposure_length)
            self.window.grabPanel.run_timer_in_background(exposure_length)
            # Show image
            self.window.imagPanel.image_queue.put((focus_image, "focus %i" % idx))
            # Store the image so it can be manually saved
            header = fits.header.Header()
            self.window.flicamera.fill_header(header)
            header["IMAGETYP"] = 'Focus Frame'
            self.window.funcPanel.last_data_and_header = (focus_image, header)
        time.sleep(2)
        # Unlock func and grab panels
        self.window.funcPanel.unlock()
        self.window.grabPanel.set_idle_mode()

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()


class SetNextWindow(Tk.Frame):
    """
    The window to configure next exposition parameters
    """
    def __init__(self, window):
        self.window = window
        self.top = Tk.Toplevel(self.window.root)
        self.top.title("Set next exposure")
        self.top.geometry('+%i+%i' % (self.window.root.winfo_x()+80,
                                      self.window.root.winfo_y()+80))
        self.top.attributes('-topmost', 'true')
        self.top.resizable(0, 0)
        self.window.popup_exists = True

        # Next photometric filter
        Tk.Label(self.top, text="Next photometric filter:").grid(column=0, row=0, sticky=Tk.E, padx=5, pady=5)
        self.phot_filter_value = Tk.StringVar()
        self.phot_filter_value.set(self.window.devicesPanel.filterwheel_photo_controls.device.current_filter_name)
        for col, filter_name in enumerate("UBVRI"):
            Tk.Radiobutton(self.top, text=filter_name, variable=self.phot_filter_value,
                           value=filter_name).grid(column=col+1, row=0)

        # Next polarimetric filter
        Tk.Label(self.top, text="Next polarimetric filter:").grid(column=0, row=1, sticky=Tk.E, padx=5, pady=5)
        self.polar_filter_value = Tk.StringVar()
        self.polar_filter_value.set(self.window.devicesPanel.filterwheel_polar_controls.device.current_filter_name)
        for col, filter_name in enumerate(["X", "Y", "Empty"]):
            Tk.Radiobutton(self.top, text=filter_name, variable=self.polar_filter_value,
                           value=filter_name).grid(column=col+1, row=1)

        # Next exposure value
        Tk.Label(self.top, text="Next exposure: ").grid(column=0, row=2, sticky=Tk.E, padx=5, pady=5)
        self.exposure_value = Tk.StringVar()
        self.exposure_value.set(self.window.grabPanel.exposure_value.get())
        self.exposure_entry = Tk.Entry(self.top, textvariable=self.exposure_value, width=3)
        self.exposure_entry.bind("<Return>", lambda e: self.set_next())
        self.exposure_entry.grid(column=1, row=2)

        # Next count value
        Tk.Label(self.top, text="Next count: ").grid(column=0, row=3, sticky=Tk.E, padx=5, pady=5)
        self.count_value = Tk.StringVar()
        self.count_value.set(self.window.grabPanel.count_value.get())
        self.count_entry = Tk.Entry(self.top, textvariable=self.count_value, width=3)
        self.count_entry.bind("<Return>", lambda e: self.set_next())
        self.count_entry.grid(column=1, row=3)

        # Next starting index
        Tk.Label(self.top, text="Next start idx: ").grid(column=0, row=4, sticky=Tk.E, padx=5, pady=5)
        self.start_index_value = Tk.StringVar()
        self.start_index_value.set(int(self.window.grabPanel.start_idx_value.get()))
        self.start_index_entry = Tk.Entry(self.top, textvariable=self.start_index_value, width=3)
        self.start_index_entry.bind("<Return>", lambda e: self.set_next())
        self.start_index_entry.grid(column=1, row=4)

        # Set button
        Tk.Button(self.top, text="Set", command=self.set_next).grid(column=0, row=5)

        # Reset button
        Tk.Button(self.top, text="Reset", command=self.reset).grid(column=1, row=5)

        # Cancel button
        Tk.Button(self.top, text="Cancel", command=self.onclose).grid(column=2, row=5, columnspan=2)

    def validate(self):
        try:
            exposure = float(self.exposure_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong exposure value!")
            return False
        if exposure <= 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Exposure length must be positive!")
            return False
        try:
            count = float(self.count_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning", message="Wrong count value!")
            return False
        if count <= 0:
            messagebox.showwarning(parent=self.top, title="Warning", message="Count value must be positive!")
            return False
        try:
            int(self.start_index_value.get())
        except ValueError:
            messagebox.showwarning(parent=self.top, title="Warning",
                                   message="Start index value must be positive integer!")
            return False
        return True

    def set_next(self):
        if self.validate() is False:
            return
        next_exposure_parameters = {}
        next_exposure_parameters["photo"] = self.phot_filter_value.get()
        next_exposure_parameters["polar"] = self.polar_filter_value.get()
        next_exposure_parameters["exposure"] = self.exposure_value.get()
        next_exposure_parameters["count"] = self.count_value.get()
        next_exposure_parameters["index"] = self.start_index_value.get()
        self.window.grabPanel.setup_next_exposure(next_exposure_parameters)
        self.window.statusPanel.set_next_exposure_info(next_exposure_parameters)
        self.onclose()

    def reset(self):
        self.window.grabPanel.setup_next_exposure(None)
        self.window.statusPanel.set_next_exposure_info(None)
        self.onclose()

    def onclose(self):
        self.window.popup_exists = False
        self.top.destroy()
