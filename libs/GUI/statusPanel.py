#! /usr/bin/env python

import tkinter as Tk
import time
import threading
import os

if os.name == 'nt':
    PASSIVE_COLOR = "SystemButtonFace"
else:
    PASSIVE_COLOR = "#d9d9d9"


class StatusPanel(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.panel = Tk.LabelFrame(self.window.root, text="Info")
        self.panel.grid(column=1, row=5, sticky=Tk.S+Tk.W+Tk.E, padx=5)

        # Define some variables to show varoius parameters:
        self.cooler_cold_part_value = Tk.StringVar()
        self.cooler_hot_part_value = Tk.StringVar()
        self.cooler_power_value = Tk.StringVar()
        self.shutter_status_value = Tk.StringVar()
        self.camera_status_value = Tk.StringVar()
        self.intensity_value = Tk.StringVar()
        self.next_filters_value = Tk.StringVar()
        self.next_filters_value.set("not set")
        self.next_exposure_value = Tk.StringVar()
        self.next_exposure_value.set("not set")
        self.next_index_value = Tk.StringVar()
        self.next_index_value.set("not set")
        self.camera_status_value.set("Idle")
        self.outside_temperature_value = Tk.StringVar()
        self.outside_temperature_value.set("--")
        # Set the status variable to a camera's local variable, so it can tell
        # the control panel what it is doing now
        self.window.flicamera.camera_status = self.camera_status_value

        # Camera status
        Tk.Label(self.panel, text="Camera:").grid(column=0, row=0, sticky=Tk.W)
        Tk.Label(self.panel, textvariable=self.camera_status_value, anchor=Tk.W,
                 width=20).grid(column=1, row=0, sticky=Tk.W)

        # Cooler status
        Tk.Label(self.panel, text="Cooler:").grid(column=0, row=1, sticky=Tk.W)
        # Cooler status consista of several values, which we would like to control
        # separately, so let's make a new Frame to pack them
        self.cooler_status_panel = Tk.Frame(self.panel)
        self.cooler_status_panel.grid(column=1, row=1, sticky=Tk.W)
        # Cooler cold part label
        self.cooler_cold_part_label = Tk.Label(self.cooler_status_panel, width=4, justify=Tk.RIGHT,
                                               textvariable=self.cooler_cold_part_value)
        self.cooler_cold_part_label.grid(column=0, row=0, sticky=Tk.E)
        # Separator
        Tk.Label(self.cooler_status_panel, text="/").grid(column=1, row=0)
        # Cooler hot part label
        self.cooler_hot_part_label = Tk.Label(self.cooler_status_panel, textvariable=self.cooler_hot_part_value)
        self.cooler_hot_part_label.grid(column=2, row=0)
        # Cooler power label
        self.cooler_power_label = Tk.Label(self.cooler_status_panel, textvariable=self.cooler_power_value)
        self.cooler_power_label.grid(column=3, row=0)

        # Intensity
        Tk.Label(self.panel, text="Pointer:").grid(column=0, row=2, sticky=Tk.W)
        Tk.Label(self.panel, textvariable=self.intensity_value, anchor=Tk.W, width=20).grid(column=1, row=2)

        # Outside temperature measured by focuser-rotator onboard thermometer
        Tk.Label(self.panel, text="T(outside):").grid(column=0, row=3, sticky=Tk.W)
        Tk.Label(self.panel, textvariable=self.outside_temperature_value, anchor=Tk.W, width=20).grid(column=1, row=3)

        # Next exposure parameters
        Tk.Label(self.panel, text="Next filters: ").grid(column=0, row=4, sticky=Tk.W)
        Tk.Label(self.panel, textvariable=self.next_filters_value, anchor=Tk.W).grid(column=1, row=4, sticky=Tk.W)
        Tk.Label(self.panel, text="Next exposure: ").grid(column=0, row=5, sticky=Tk.W)
        Tk.Label(self.panel, textvariable=self.next_exposure_value, anchor=Tk.W).grid(column=1, row=5, sticky=Tk.W)
        # Tk.Label(self.panel, text="Next index: ").grid(column=0, row=7, sticky=Tk.W)
        # Tk.Label(self.panel, textvariable=self.next_index_value, anchor=Tk.W).grid(column=1, row=7, sticky=Tk.W)
        # start a background job for showing the camera temperatures
        threading.Thread(target=self.update_info).start()

    def update_info(self):
        """
        Every two secons fetch the temperature parameters and show them in the status section
        """
        while 1:
            if self.window.alive is False:
                print("update_info stopped")
                return
            # Update camera cooler temperatures and power
            cold, hot, power = self.window.flicamera.get_temperatures()
            # If temperatures of power are out of range show them with color, to draw
            # the observers attention
            if power < 80:
                self.cooler_power_label.config(bg=PASSIVE_COLOR)
            elif 80 <= power < 90:
                self.cooler_power_label.config(bg="gold")
            else:
                self.cooler_power_label.config(bg="tomato")
            if hot < 40:
                self.cooler_hot_part_label.config(bg=PASSIVE_COLOR)
            else:
                self.cooler_hot_part_label.config(bg="tomato")
            if abs(self.window.flicamera.current_set_temperature - cold) < 0.5:
                self.cooler_cold_part_label.config(bg=PASSIVE_COLOR)
            elif 1 < abs(self.window.flicamera.current_set_temperature - cold) < 1:
                self.cooler_cold_part_label.config(bg="gold")
            else:
                self.cooler_cold_part_label.config(bg="tomato")
            self.cooler_cold_part_value.set("%-2.1f" % cold)
            self.cooler_hot_part_value.set("%1.1f" % hot)
            power_string = "(%1.1f" % power
            power_string += "%)"
            self.cooler_power_value.set(power_string)
            # Update outside temperature
            temp = self.window.focuser_rotator.focuser_status["CurrTemp"]
            string = "%1.1f\u00B0" % temp
            self.outside_temperature_value.set(string)
            time.sleep(2)

    def set_next_exposure_info(self, next_exposure_parameters=None):
        if next_exposure_parameters is None:
            filters_string = 'not set'
            expo_string = 'not set'
        else:
            filters_string = "%s/%s" % (next_exposure_parameters['photo'],
                                        next_exposure_parameters['polar'])
            expo_string = "time=%s count=%s idx=%s" % (next_exposure_parameters['exposure'],
                                                       next_exposure_parameters['count'],
                                                       next_exposure_parameters['index'])

        self.next_filters_value.set(filters_string)
        self.next_exposure_value.set(expo_string)
