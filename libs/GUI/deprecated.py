class CFW8GUI(Tk.Frame):
    """
    GUI for controlling CFW8 wheel device
    """
    def __init__(self, focus_wheel_panel):
        self.window = focus_wheel_panel.window
        self.cfw8_panel = Tk.Frame(focus_wheel_panel.panel)
        self.cfw8_panel.grid(column=0, row=0, padx=5, pady=3)
        Tk.Label(self.cfw8_panel, text="CFW-8 wheel",
                 font=("Times", 12, "bold")).grid(column=0, row=0, columnspan=2, sticky=Tk.W)
        self.status_message = Tk.StringVar()
        self.status_message.set("Starting")
        Tk.Label(self.cfw8_panel, textvariable=self.status_message, font=("Times", 10),
                 justify=Tk.LEFT).grid(column=2, row=0, columnspan=2, sticky=Tk.W)

        # Buttons
        self.buttons = {}
        for idx, filter_name in enumerate(["U", "B", "V", "R", "I"]):
            button = Tk.Button(self.cfw8_panel, text=filter_name, width=3,
                               font=("Times", 12, "bold"), state='disabled',
                               command=lambda x=filter_name: self.on_click(x))
            self.buttons[filter_name] = button
            self.buttons[filter_name].grid(column=idx, row=1, padx=5)

        # The CFW8 GUI is created with disabled because it is possible that the
        # device is not ready at this moment. In order not to lock the rest of the
        # GUI creation, we will wait for the device in the background and then
        # unlock the buttons
        Thread(target=self.wait).start()

    def wait(self):
        """
        Function waits untill the device is ready to use and then
        enables the GUI
        """
        while 1:
            if self.window.cfw8.is_ready:
                break
            time.sleep(0.05)
        self.buttons[self.window.cfw8.current_filter].config(bg=ACTIVE_BUTTON_COLOR)
        self.status_message.set("Ready")
        self.unlock()

    def lock(self):
        """
        Lock all buttons
        """
        for button in self.buttons.values():
            button.config(state="disabled")

    def unlock(self):
        """
        Unlock all buttons
        """
        for button in self.buttons.values():
            button.config(state="normal")

    def on_click(self, filter_name):
        # When the user switches the filter, ze probably wants to start a
        # new sequence from so we can set starting index in the
        # grab panel equal to zero
        self.window.grabPanel.start_idx_value.set("0")
        # Start filter switching in a background thread
        Thread(target=self.select_filter, args=(filter_name,)).start()

    def select_filter(self, filter_name):
        self.status_message.set("Moving")
        self.lock()
        self.buttons[self.window.cfw8.current_filter].config(bg=PASSIVE_BUTTON_COLOR)
        self.window.cfw8.set_filter(filter_name)
        self.buttons[self.window.cfw8.current_filter].config(bg=ACTIVE_BUTTON_COLOR)
        self.unlock()
        self.status_message.set("Ready")



class FocuserGUI(Tk.Frame):
    """
    GUI for the focuser device
    """
    def __init__(self, focus_wheel_panel):
        self.window = focus_wheel_panel.window
        self.focuser_panel = Tk.Frame(focus_wheel_panel.panel)
        self.focuser_panel.pack()  # grid(column=0, row=2, padx=5, pady=3, sticky=Tk.W)

        self.focus_position_value = Tk.StringVar()
        self.focus_position_value.set("Current location: %i" % self.window.focuser.position)
        Tk.Label(self.focuser_panel, text="Focuser", font=("Times", 12, "bold"),
                 justify=Tk.LEFT).grid(column=0, row=0, sticky=Tk.W)
        Tk.Label(self.focuser_panel, textvariable=self.focus_position_value,
                 font=("Times", 12)).grid(column=0, row=1, columnspan=3, sticky=Tk.W)

        # Reset button
        self.reset_button = Tk.Button(self.focuser_panel, text="Reset", font=("Arial", 10),
                                      command=self.reset)
        self.reset_button.grid(column=0, row=2, sticky=Tk.E, padx=10)

        # Move left button
        self.move_left_button = Tk.Button(self.focuser_panel, text=u"\u2212", font=("Arial", 16, "bold"),
                                          command=lambda: self.on_click("left"))
        self.move_left_button.grid(column=1, row=2, sticky=Tk.E, padx=10)

        # Move step entry
        self.step_variable = Tk.StringVar()
        self.step_variable.set(500)
        self.move_step_entry = Tk.Entry(self.focuser_panel, textvariable=self.step_variable, width=6, justify=Tk.RIGHT)
        self.move_step_entry.grid(column=2, row=2)

        # Move right button
        self.move_right_button = Tk.Button(self.focuser_panel, text="+", font=("Arial", 16, "bold"),
                                           command=lambda: self.on_click("right"))
        self.move_right_button.grid(column=3, row=2, sticky=Tk.W, padx=10)

        # Status message
        self.status_message = Tk.StringVar()
        self.status_message.set("OK")
        Tk.Label(self.focuser_panel, textvariable=self.status_message, wraplength=50).grid(column=4, row=2)

    def lock(self):
        """
        Lock all active GUI elements
        """
        self.move_left_button.config(state='disabled')
        self.move_right_button.config(state='disabled')
        self.move_step_entry.config(state='disabled')
        self.reset_button.config(state='disabled')

    def unlock(self):
        """
        Unlock GUI elements
        """
        self.move_left_button.config(state='normal')
        self.move_right_button.config(state='normal')
        self.move_step_entry.config(state='normal')
        self.reset_button.config(state='normal')

    def on_click(self, direction):
        try:
            step = float(self.step_variable.get())
        except ValueError:
            self.status_message.set("Wrong step")
            return
        if step > 1000:
            self.status_message.set("Step is too big")
            return
        Thread(target=self.move, args=(step, direction)).start()

    def move(self, step, direction):
        self.status_message.set("Moving")
        self.lock()  # Lock interface while moving
        self.window.focuser.move(step, direction)
        self.status_message.set("OK")
        self.focus_position_value.set("Current location: %i" % self.window.focuser.position)
        self.unlock()

    def reset(self):
        def f():
            self.lock()
            self.window.focuser.reset()
            self.status_message.set("OK")
            self.focus_position_value.set("Current location: %i" % self.window.focuser.position)
            self.unlock()
        Thread(target=f).start()


class SerialDevice(object):
    def __init__(self, address, baudrate):
        self.device = Serial(address, baudrate=baudrate, timeout=0.2)

    def send_command(self, command):
        self.device.flushOutput()
        self.device.write(str.encode(command))

    def get_response(self, lines_desired):
        """Read output until end_str is found"""
        response = []
        while len(response) < lines_desired:
            new_line = str(self.device.readline())
            response.append(new_line)
            time.sleep(0.05)
        return response

    def readline(self):
        byte_line = self.device.readline()
        return byte_line.decode("utf-8").strip()

    def flush(self):
        self.device.read(self.device.in_waiting)

    def close(self):
        self.device.close()




class FakeDevice(object):
    def __init__(self):
        pass

    def close(self):
        time.sleep(0.5)


class CFW8(object):
    def __init__(self):
        # Pack buttons for filter selection
        self.filter_list = ["U", "B", "V", "R", "I"]
        self.num_of_filters = len(self.filter_list)
        self.current_filter = "U"
        self.moving = False

        # Initialise wheel
        if os.name == "nt":
            try:
                self.device = SerialDevice("COM6", baudrate=115200)
            except SerialException:
                raise DeviceIsNotAvailableError("CFW8 is not available")
        else:
            self.device = FakeDevice()
        self.roll_filters()
        self.is_alive = True
        self.is_ready = False

    def roll_filters(self):
        def f():
            for filter_name in self.filter_list[1:]:
                self.set_filter(filter_name)
            self.set_filter(self.filter_list[0])
            self.is_ready = True
        Thread(target=f).start()

    def shutdown(self):
        def f():
            self.set_filter("U")
            self.device.close()
            self.is_alive = False
        Thread(target=f).start()

    def callback(self, filter_name):
        if self.moving is False:
            thread = Thread(target=self.set_filter, args=(filter_name))
            thread.start()

    def set_filter(self, filter_name):
        """
        Set a particular filter to a filter wheel
        """
        print("setting %s" % filter_name)
        self.moving = True
        while self.current_filter != filter_name:
            current_filter_idx = self.filter_list.index(self.current_filter)
            next_filter = self.filter_list[(current_filter_idx + 1) % self.num_of_filters]
            if os.name == 'nt':
                # Device works only on windows
                self.device.send_command(next_filter)
                while 1:
                    line = self.device.readline()
                    if "done" in line:
                        #self.statusMessage.set("OK")
                        break
                    elif "Error" in line:
                        #self.statusMessage.set("Error")
                        self.moving = False
                        break
            else:
                time.sleep(0.5)
            self.current_filter = next_filter
