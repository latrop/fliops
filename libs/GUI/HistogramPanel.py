#! /usr/bin/env python

import os
import tkinter as Tk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib import gridspec
import numpy as np


class HistogramPanel(Tk.Frame):
    def __init__(self, window):
        self.window = window
        self.panel = Tk.Frame(self.window.root)
        self.panel.grid(column=1, row=4, sticky=Tk.S+Tk.W+Tk.E, padx=5)
        self.mainGraph = plt.figure(figsize=(3.25, 1.15), dpi=96)
        if os.name == "nt":
            self.mainGraph.patch.set_facecolor('0.93')
        else:
            self.mainGraph.patch.set_facecolor(self.window.root.cget('bg'))
        self.canvas = FigureCanvasTkAgg(self.mainGraph, master=self.panel)
        self.gs = gridspec.GridSpec(4, 1, height_ratios=[9, 0.1, 1, 1], hspace=0.1,
                                    left=0.05, right=0.95, top=0.99, bottom=0.1)
        self.canvas.mpl_connect('button_press_event', self.onclick)
        self.canvas.mpl_connect('scroll_event', self.onscroll)
        # Main histogram figure
        self.hist_fig = plt.subplot(self.gs[0])
        self.hist_fig.axes.set_xticks([])
        self.hist_fig.axes.set_yticks([])
        self.hist_plot_instance = None
        plt.subplot(self.gs[1]).axes.set_visible(False)
        # Area for upper bound click
        self.upper_fig = plt.subplot(self.gs[2])
        self.upper_fig.axes.set_xticks([])
        self.upper_fig.axes.set_yticks([])
        self.upper_plot_instances = []
        # Area for lower bound click
        self.lower_fig = plt.subplot(self.gs[3])
        self.lower_fig.axes.set_xticks([])
        self.hist_fig.tick_params(axis="x", direction="in", pad=-20, rotation=45, labelsize=6)
        self.lower_fig.axes.set_yticks([])
        self.lower_plot_instances = []
        self.tk_widget = self.canvas.get_tk_widget()
        self.tk_widget.grid(column=0, row=0)
        # self.mainGraph.tight_layout(pad=0.2)
        self.canvas.draw()

    def update_plot(self):
        data = self.window.imagPanel.data.flatten()
        # Show histogram
        xlim = (np.percentile(data, 1),
        		max(np.percentile(data, 99.9),
        		self.window.imagPanel.meanValue + 5.25 * self.window.imagPanel.stdValue))
        if self.hist_plot_instance is not None:
            self.hist_plot_instance[0].remove()
            self.hist_plot_instance = None
        yhist, xhist, self.hist_plot_instance = self.hist_fig.hist(data, histtype='step', color="k", bins=75, range=xlim)
        self.hist_fig.axes.set_xlim(*xlim)
        self.hist_fig.axes.set_ylim(0, 1.05 * yhist.max())
        # Determine ticks location
        dx = xlim[1] - xlim[0]
        x_ticks = np.linspace(xlim[0], xlim[1], 10)
        self.hist_fig.axes.set_xticks((int(x_ticks[1]),
                                       int(self.window.imagPanel.meanValue),
                                       int(x_ticks[-2]))) #,
                                       # ha='left')
        # Show upper limit
        while self.upper_plot_instances:
            self.upper_plot_instances.pop().remove()
        self.upper_plot_instances.append(self.upper_fig.axvline(x=self.window.imagPanel.upper_limit, color="k"))
        an = self.upper_fig.annotate(text=f"{int(self.window.imagPanel.upper_limit)}", xycoords="data",
                                     xy=(self.window.imagPanel.upper_limit-dx*0.01, 0.45),
                                     fontsize=5,
                                     horizontalalignment='right',
                                     verticalalignment='center')
        self.upper_plot_instances.append(an)
        self.upper_fig.axes.set_xlim(*xlim)
        self.upper_fig.axes.set_ylim(0, 1)

        # Show lower limit
        while self.lower_plot_instances:
            self.lower_plot_instances.pop().remove()
        self.lower_plot_instances.append(self.lower_fig.axvline(x=self.window.imagPanel.lower_limit, color="k"))
        an = self.lower_fig.annotate(text=f"{int(self.window.imagPanel.lower_limit)}", xycoords="data",
                                     xy=(self.window.imagPanel.lower_limit+dx*0.01, 0.425),
                                     fontsize=5,
                                     horizontalalignment='left',
                                     verticalalignment='center')
        self.lower_plot_instances.append(an)
        self.lower_fig.axes.set_xlim(*xlim)

        self.canvas.draw()

    def onclick(self, event):
        if self.window.imagPanel.lower_limit is None:
            # No current limits mean that there is no image loaded yet
            return
        new_value = event.xdata
        if event.inaxes == self.lower_fig:
            # Click was made on the low fig plot
            if new_value < self.window.imagPanel.upper_limit:
                self.window.imagPanel.lower_limit = new_value
        elif event.inaxes == self.upper_fig:
            # Click was made on the upper fig plot
            if new_value > self.window.imagPanel.lower_limit:
                self.window.imagPanel.upper_limit = new_value
        # Rerun image plot
        self.window.imagPanel.image_queue.put(("Same", None))

    def onscroll(self, event):
        if self.window.imagPanel.lower_limit is None:
            # No current limits mean that there is no image loaded yet
            return
        delta = (self.window.imagPanel.upper_limit - self.window.imagPanel.lower_limit) * 0.05
        if event.inaxes == self.lower_fig:
            if event.button == "up":
                new_value = self.window.imagPanel.lower_limit + delta
            else:
                new_value = self.window.imagPanel.lower_limit - delta
            if new_value < self.window.imagPanel.upper_limit:
                self.window.imagPanel.lower_limit = new_value
        elif event.inaxes == self.upper_fig:
            # Click was made on the upper fig plot
            if event.button == "up":
                new_value = self.window.imagPanel.upper_limit + delta
            else:
                new_value = self.window.imagPanel.upper_limit - delta
            if new_value > self.window.imagPanel.lower_limit:
                self.window.imagPanel.upper_limit = new_value
        # Rerun image plot
        self.window.imagPanel.image_queue.put(("Same", None))
